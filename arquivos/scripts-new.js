var header = {
	'Accept': 'application/json',
	'REST-range': 'resources=0-10',
	'Content-Type': 'application/json; charset=utf-8'
};

var insertMasterData = function (ENT, loja, dados, fn) {
	$.ajax({
		url: 'https://api.vtexcrm.com.br/' + loja + '/dataentities/' + ENT + '/documents/',
		type: 'PATCH',
		data: dados,
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {}
	});
};

var selectMasterData = function (ENT, loja, params, fn) {
	$.ajax({
		url: 'https://api.vtexcrm.com.br/' + loja + '/dataentities/' + ENT + '/search?' + params,
		type: 'GET',
		headers: header,
		success: function (res) {
			fn(res);
		},
		error: function (res) {}
	});
};

$mobile = 768;

var geral = (function () {

	//APENAS HOME
	var desktop = {
		pop_up_promo: function () {
			//ATIVA POP UP E OVERLAY
			$('body.home .popup-promo, body.home #overlay').addClass('active strong');

			//CLICANDO NO OVERLAY FECHA POPUP, CARRINHO, BODY, HEADER E MENU DEPARTAMENTO MOBILE
			$('.close-popup-promo, #overlay').on('click', function () {
				$('.popup-promo, #overlay, #cart-lateral, .search-multiple-navigator, header .row-3 nav.active, body').removeClass('active strong');
			});

			//ENVIA OS DADOS PARA O MASTER DATA
			$('.popup-promo-form').on('submit', function (e) {
				e.preventDefault();

				var dados = {
					name: $('.popup-promo-form input[name=name]').val(),
					email: $('.popup-promo-form input[name=email]').val()
				}

				console.log(dados);

				var json_dados = JSON.stringify(dados);
				console.log(json_dados);


				insertMasterData("PP", 'anyany', json_dados, function (res) {
					$('.popup-promo-form').html('<h3>Obrigado por se inscrever em nossa newsletter em breve você receberá novidades.</h3>');
					console.log(res);

					setTimeout(function () {
						$('.popup-promo, #overlay').removeClass('active');
					}, 3000);
				});
			});
		},

		remove_helper: function () {
			$('li.helperComplement').remove();
		},

		slick_banner_principal: function () {
			$('.home .fullbanner ul').slick({
				infinite: true,
				autoplay: true,
				autoplaySpeed: 4000,
				dots: true,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1,
							arrows: false
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							arrows: false
						}
					}
				]
			});

			//EVITA A QUEBRA DO BANNER AO INICIAR A HOME
			$('.home .fullbanner.desktop').addClass('container');
		},

		slick_prateleiras: function () {
			$('.home .collection_1 ul, .home .collection_2 .prateleira ul').slick({
				infinite: true,
				autoplay: true,
				autoplaySpeed: 4000,
				dots: false,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					}
				]
			});

			$('.home .collection_3 ul').slick({
				infinite: true,
				autoplay: false,
				autoplaySpeed: 4000,
				dots: false,
				arrows: true,
				slidesToShow: 4,
				slidesToScroll: 1,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					}
				]
			});
		},

		header_hover: function () {
			$('header .row-3 nav ul li span+div').mouseover(function () {
					$(this).parents('li').find('span').addClass('active');
				})
				.mouseout(function () {
					$(this).parents('li').find('span').removeClass('active');
				});
		},

		instafeed: function () {
			if ($('#instafeed').length > 0) {
				var feed = new Instafeed({
					get: 'user',
					userId: 348905890,
					accessToken: '348905890.1677ed0.2274344589004e979e43f6ec47ca6241',
					target: 'instafeed',
					limit: '4',
					resolution: 'standard_resolution',
					template: '<article><a href="{{link}}" class="insta" target="_blank" ><img src="{{image}}" /><div class="likes">&hearts; {{likes}}</div></a></article>',
					after: function () {
						var el = $('#instafeed');
						if (el.classList)
							el.classList.add('show');
						else
							el.className += ' ' + 'show';

						if ($(window).width() < 768) {
							$('#instafeed').slick({
								dots: false,
								arrows: false,
								infinite: false,
								autoplay: true,
								speed: 1000,
								slidesToShow: 1,
								slidesToScroll: 1,
								responsive: [{
										breakpoint: 1024,
										settings: {
											slidesToShow: 1,
											slidesToScroll: 1,
											infinite: true,
											arrows: true
										}
									},
									{
										breakpoint: 600,
										settings: {
											slidesToShow: 1,
											slidesToScroll: 1,
											arrows: true
										}
									}
								]
							});
						}
					}
				});

				feed.run();
			}
		},

		newsletter_value: function () {
			$('#newsletterButtonOK').val('cadastrar');
		}
	};

	var mobile = {
		header_hamburger: function () {
			$('header .hamburger').on('click', function () {
				$('header nav, #overlay, body').toggleClass('active');
			});

			$('header nav .btn-fechar').on('click', function () {
				$('header .hamburger').trigger('click');
			});
		},

		header_busca: function () {
			$('header .ico-lupa').on('click', function () {
				$('.busca-mob').toggleClass('active');
			});

			$('header .busca-mob .ico-close').on('click', function () {
				$('header .ico-lupa').trigger('click');
			});
		},

		header_submenu: function () {
			$('header .row-3 nav ul li span a').on('click', function (e) {
				e.preventDefault();
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).parents('span').next('div').slideUp();
				} else {
					$('header .row-3 nav ul li span a').removeClass('active');
					$('header .row-3 nav ul li span + div').slideUp();

					$(this).addClass('active');
					$(this).parents('span').next('div').slideDown();
				}
			});
		},

		footer_menu: function () {
			$('.menu-footer p').on('click', function () {
				$(this).next('ul').slideToggle();
			});
		},

		main_tipbar: function () {
			$('ul.tipbar').slick({
				infinite: true,
				autoplay: true,
				autoplaySpeed: 4000,
				dots: false,
				arrows: false
			});
		}
	}

	desktop.pop_up_promo();
	desktop.remove_helper();
	desktop.slick_banner_principal();
	desktop.slick_prateleiras();
	desktop.header_hover();
	desktop.instafeed();
	desktop.newsletter_value();

	if ($('body').width() < $mobile) {
		mobile.header_hamburger();
		mobile.header_busca();
		mobile.header_submenu();
		mobile.footer_menu();
		mobile.main_tipbar();
	}
})();

var produto = (function () {
	var desktop = {
		validate_price: function () {
			//EXIBE VALOR POR
			if ($('.valor-dividido.price-installments').css('display') === 'none') {
				$('em.valor-por.price-best-price').attr('style', 'display: block!important');
			}

			//EXIBE VALOR POR QND MUDA SKU
			$('.sku-selector-container ul.topic.Tamanho li.skuList span label').on('click', function () {
				//REMOVE MENSAGEM DE ESTOQUE
				$('.produto .row-1 .no-estoque').removeClass('active');

				setTimeout(() => {
					if ($('.valor-dividido.price-installments').css('display') === 'none') {
						$('em.valor-por.price-best-price').attr('style', 'display: block!important');
					}
				}, 500);
			});
		},

		item_unavailable: function () {
			$(document).ready(function () {
				if ($('body.produto').length === 1) {
					function e() {
						var e = {};
						vtexjs.catalog.getCurrentProductWithVariations().done(function (i) {
							$.each(i.skus, function (i, t) {
								var a = t.values[0];
								e[a] || (e[a] = 0), t.available && e[a]++
							});
							for (var t in e) 0 == e[t] && $(".selection_prod li label.skuespec_Tamanho_opcao_" + t).addClass("item_unavailable")
						})
					}

					function i() {
						clearTimeout(window.timeOutProduct), window.timeOutProduct = setTimeout(function () {
							e()
						}, 100)
					}
					i();

					$('.produto main .row-1 .selection_prod .sku-selector-container ul.topic.Tamanho li.skuList span label').on('click', function () {
						i();
					});
				}
			});
		},

		count: function () {
			$('.count').on('click', function (e) {
				e.preventDefault();
				if ($(this).hasClass('count-mais')) {
					var qtd_produtos = $('.box_count .input input').attr('value');
					qtd_produtos++;
				} else if ($(this).hasClass('count-menos')) {
					var qtd_produtos = $('.box_count .input input').attr('value');

					if (qtd_produtos > 1) {
						qtd_produtos--;
					}
				}

				$('.box_count .input input').attr('value', qtd_produtos);
			});
		},

		get_size_image: function () {

			var width = document.querySelector('#image > a > div > div.zoomWindow > div > div.zoomWrapperImage > img').naturalWidth;
			var height = document.querySelector('#image > a > div > div.zoomWindow > div > div.zoomWrapperImage > img').naturalHeight;

			$('.zoomWrapperImage img').css('width', width);
			$('.zoomWrapperImage img').css('height', height);
		},

		accordion_prod: function () {
			$('.accordion_prod p').on('click', function () {
				$(this).next('div').slideToggle();
				$(this).find('i').toggleClass('active');
			});
		}
	}

	var mobile = {
		add_height: function () {
			$(window).load(function () {
				//ADICIONA ALTURA NO THUMB
				var height = $('.produto main .row-1 .apresentacao #show .thumbs li img').height();
				$('.produto main .row-1 .apresentacao #show .thumbs').css('height', height);
			});
		},

		image_principal: function () {
			$('.produto .thumbs').slick({
				infinite: true,
				autoplay: false,
				autoplaySpeed: 3000,
				dots: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
			});
		},

		slick: function () {
			$('.produto .row-2 .prateleira ul').slick({
				infinite: true,
				autoplay: false,
				autoplaySpeed: 4000,
				dots: false,
				arrows: true,
				slidesToShow: 4,
				slidesToScroll: 1,
				responsive: [{
						breakpoint: 1024,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					}
				]
			});
		},

		build_thumb: function () {
			//CONSTROI THUMB  APOS MUDAR DE SKU
			$('.sku-selector-container ul.topic.Tamanho li.skuList span label').on('click', function () {
				$('.thumbs').removeClass('slick-initialized slick-slider');

				setTimeout(() => {
					mobile.image_principal();
				}, 1000);
			});
		}
	}

	desktop.validate_price();
	desktop.item_unavailable();
	desktop.count();
	//desktop.get_size_image();
	desktop.accordion_prod();

	if ($('body').width() < $mobile) {
		mobile.image_principal();
		mobile.slick();
		mobile.add_height();
		mobile.build_thumb();
	}
})();

var departamento = (function () {
	var desktop = {
		smartResearch: function () {
			$(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch();
		}
	}

	var mobile = {
		optionsFilter: function () {
			$('.departamento main .search-multiple-navigator fieldset h5').on('click', function () {
				$(this).toggleClass('active');
				$(this).next('div').slideToggle();
			});
		},

		editSelect: function () {
			$('.orderBy select option:first-child').text('Ordernar por');
		},

		filtrar: function () {
			$('.departamento main .main .sub').after('<span class="open_filtro">Filtrar</span>');
			$('.open_filtro').on('click', function (e) {
				e.preventDefault();
				$('.search-multiple-navigator, #overlay, body').addClass('active');
			});

			//BOTÃO FILTRAR - MOBILE
			$('.search-multiple-navigator').append('<ul class="search-multiple-navigator-footer"><li><a href="" class="btn_cancelar">Cancelar</a></li><li><a href="" class="btn_filtrar">Aplicar filtros</a></li></ul>');
			$(document).on('click', '.btn_cancelar, .btn_filtrar', function (e) {
				e.preventDefault();
				$('.search-multiple-navigator, #overlay, body').removeClass('active');
			});
		}
	}

	if ($('body.departamento').length > 0 || $('body.categoria').length > 0) {
		desktop.smartResearch();

		if ($('body').width() < $mobile) {
			mobile.filtrar();
			mobile.optionsFilter();
			mobile.editSelect();
		}
	}
})();

var institucional = (function () {
	var functions = {
		form: function () {
			if ( $('body').hasClass('multimarcas') ) {
				document.getElementById('btn-enviar-dados').addEventListener('click', getDados)
				maskCampos();
	
				var dadosFormulario = {};
				var mensagem = '';
	
				function maskCampos() {
					$('#telefone').mask('(00) 0000-00009')
					$('#celular').mask('(00) 0000-00009')
					$('#cnpj').mask('99.999.999/9999-99')
					$('#cep').mask('00000-000')
				}
	
				function getDados() {
					event.preventDefault();
	
					dadosFormulario = {
						nome: document.getElementById('nome').value.trim(),
						telefone: document.getElementById('telefone').value.trim(),
						celular: document.getElementById('celular').value.trim(),
						email: document.getElementById('email').value.trim(),
						razaoSocial: document.getElementById('razaoSocial').value.trim(),
						nomeFantasia: document.getElementById('nomeFantasia').value.trim(),
						cnpj: document.getElementById('cnpj').value.trim(),
						endereco: document.getElementById('endereco').value.trim(),
						cidade: document.getElementById('cidade').value.trim(),
						uf: document.getElementById('uf').value.trim(),
						cep: document.getElementById('cep').value.trim(),
						marcas: document.getElementById('marcas').value,
						tempoFuncionamento: document.getElementById('tempoFuncionamento').value.trim(),
					}
	
					validacaoForm();
					enviarDados();
				}
	
				function validacaoForm() {
					document.querySelector('.inputs input').classList.remove('error');
					$('.inputs p').find('span').remove();
	
					for (const prop in dadosFormulario) {
						mensagem = prop + ' inválido ';
	
	
						console.log(dadosFormulario[prop]);
	
						if (dadosFormulario[prop] == '' && $('span.' + prop).length <= 0) {
							document.getElementById(prop).classList.add('error');
							$('#' + prop).parent().append('<span class="input-error ' + prop + '">' + ' Esse campo é obrigatório. ' + '</span>');
						} else if ((prop == 'cnpj') && (validaCnpj(dadosFormulario[prop]) == false)) {
							document.getElementById(prop).classList.add('error');
							$('#' + prop).parent().append('<span class="input-error ' + prop + '">' + mensagem + '</span>');
						} else if ((prop == 'cep') && (validaCep(dadosFormulario[prop]) == false)) {
							document.getElementById(prop).classList.add('error');
							$('#' + prop).parent().append('<span class="input-error ' + prop + '">' + mensagem + '</span>');
						} else if ((prop == 'email') && (validaEmail(dadosFormulario[prop]) == false)) {
							document.getElementById(prop).classList.add('error');
							$('#' + prop).parent().append('<span class="input-error ' + prop + '">' + mensagem + '</span>');
						} else {
							document.getElementById(prop).classList.remove('error');
							$('.inputs p').find('.' + prop).remove();
						}
					}
	
				}
	
				function validaCnpj(cnpj) {
					console.log('validando cnpj')
	
					cnpj = cnpj.replace(/[^\d]+/g, '');
	
					if (cnpj == '') return false;
	
					if (cnpj.length != 14)
						return false;
	
					// Elimina CNPJs invalidos conhecidos
					if (cnpj == "00000000000000" ||
						cnpj == "11111111111111" ||
						cnpj == "22222222222222" ||
						cnpj == "33333333333333" ||
						cnpj == "44444444444444" ||
						cnpj == "55555555555555" ||
						cnpj == "66666666666666" ||
						cnpj == "77777777777777" ||
						cnpj == "88888888888888" ||
						cnpj == "99999999999999")
						return false;
	
					// Valida DVs
					tamanho = cnpj.length - 2
					numeros = cnpj.substring(0, tamanho);
					digitos = cnpj.substring(tamanho);
					soma = 0;
					pos = tamanho - 7;
					for (i = tamanho; i >= 1; i--) {
						soma += numeros.charAt(tamanho - i) * pos--;
						if (pos < 2)
							pos = 9;
					}
					resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
					if (resultado != digitos.charAt(0))
						return false;
	
					tamanho = tamanho + 1;
					numeros = cnpj.substring(0, tamanho);
					soma = 0;
					pos = tamanho - 7;
					for (i = tamanho; i >= 1; i--) {
						soma += numeros.charAt(tamanho - i) * pos--;
						if (pos < 2)
							pos = 9;
					}
					resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
					if (resultado != digitos.charAt(1))
						return false;
	
					return true;
				}
	
				function validaCep(cep) {
	
					cep = cep.replace(/^s+|s+$/g, '');
					var objER = /^[0-9]{5}-[0-9]{3}$/;
	
					if (cep.length > 0) {
						if (objER.test(cep))
							return true;
						else
							return false;
					} else
						return false;
	
				}
	
				function validaEmail(email) {
					console.log('teste');
					if (email.value == "" ||
						email.indexOf('@') == -1 ||
						email.indexOf('.') == -1) {
						return false;
					}
				}
	
				function enviarDados() {
					var json_dados = JSON.stringify(dadosFormulario);
					console.log(json_dados)
	
					insertMasterData("MM", 'anyany', json_dados, function (res) {
						console.log(res);
						document.getElementById('success').classList.add('active');
						$('.multimarcas form').remove();
					});
				}
			}
		}
	}

	functions.form();
})();