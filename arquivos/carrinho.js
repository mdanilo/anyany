$mobile = 768;
var carrinho = (function () {

    var functions = {
        barra_frete: function () {
			if ($('body').width() < 812) {
				$('#message_frete').prependTo('#cart-lateral .content');
			}

			$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
				if ( orderForm.totalizers.length === 0 ) {
					$('#message_frete').hide();
				} else {
					$('#message_frete').show();
					var desconto = 30000 - orderForm.totalizers[0].value;
					var valor_porcento = (desconto / 30000) * 100;
	
					var valor_restante = desconto / 100;
					valor_restante = valor_restante.toFixed(2);
					valor_restante = valor_restante.replace('.',',');

					//BARRA_FRETE
					if ( orderForm.totalizers[0].value > 30000 ) {
						$('#barra_frete, #message_frete').addClass('success');
					} else {
						$('#barra_frete, #message_frete').removeClass('success');
						$('#barra_frete .column-1 .message_default strong, #message_frete .message_default strong').text('R$ '+valor_restante);
						$('#barra_frete .column-2 .barra span').css('width', valor_porcento+'%');
					}
				}
			});
        },
        
        //REMOVE MENSAGEM SEM ESTOQUE
        no_estoque: function () {
            $('.no-estoque').on('click', function () {
                $(this).removeClass('active');
            });
        },

        //MOBILE - 
        openCart: function () {
            if ($('body').width() < $mobile) {
                $('#cart-lateral .header, #cart-lateral .cart-close').on('click', function () {
                    $('#cart-lateral, #overlay, body').toggleClass('active');
                });
            }
        },

        //QUANTIDADE DE ITEMS NO CARRINHO
        qtyCart: function () {
            var quantidade = 0;

            vtexjs.checkout.getOrderForm()
                .done(function (orderForm) {

                    for (var i = orderForm.items.length - 1; i >= 0; i--) {
                        quantidade = parseInt(quantidade) + parseInt(orderForm.items[i].quantity);
                    }

                    $('#qty').text(quantidade);
                });
        },

        //HOVER NO ICONE
        toggleCart: function () {
            $('header .ico-cart > a').on('click', function (e) {
                e.preventDefault();
                $('#overlay, #cart-lateral, body').toggleClass('active');
            });

            $('#cart-lateral .cart-close').on('click', function () {
                $('header .ico-cart > a').trigger('click');
                //REMOVE MENSAGEM ADD PRODUTO
                $('#cart-message li').remove();
            });
        },

        //CALCULA FRETE
        calculateShipping: function () {
            if ($('#search-cep input[type="text"]').val() != '') {
                vtexjs.checkout.getOrderForm()
                    .then(function (orderForm) {
                        if (localStorage.getItem('cep') === null) {
                            var postalCode = $('#search-cep input[type="text"]').val();
                        } else {
                            var postalCode = localStorage.getItem('cep');
                        }

                        var country = 'BRA';
                        var address = {
                            "postalCode": postalCode,
                            "country": country
                        };

                        //EFEITO FADE
                        $('#cart-lateral .footer ul li span.frete .box-1').fadeOut(300, function () {
                            $('#cart-lateral .footer ul li span.frete .box-2').fadeIn(300);
                        });

                        return vtexjs.checkout.calculateShipping(address);
                    })
                    .done(function (orderForm) {
                        if (orderForm.totalizers.length == 0) {
                            $('#cart-lateral .value-frete').text('Você não tem itens no carrinho!');
                        } else {
                            var value_frete = orderForm.totalizers[1].value / 100;
                            value_frete = value_frete.toFixed(2).replace('.', ',').toString();
                            $('#cart-lateral .value-frete').text('R$: ' + value_frete);

                            var postalCode = $('#search-cep input[type="text"]').val();
                            localStorage.setItem('cep', postalCode);
                            $('#search-cep input[type="text"]').val(postalCode);
                        }
                    });
            }
        },

        //APOS INSERIDO - CALCULA FRETE AO CARREGAR A PG
        automaticCalculateShipping: function () {
            $(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
                if (localStorage.getItem('cep') != null) {
                    $('#search-cep input[type="text"]').val(localStorage.getItem('cep'));
                    functions.calculateShipping();
                }
            });
        },

        //EFEITO FADE DO CALCULAR
        fadeAction: function () {
            $('#cart-lateral .footer ul li span.frete a').on('click', function (e) {
                e.preventDefault();
                $('#cart-lateral .footer ul li span.frete a').fadeOut(300, function () {
                    $('#cart-lateral .footer ul li span.frete .box-1 div').fadeIn(300);
                });
            });

            //CALCULAR NOVAMENTE
            $('#cart-lateral .return-frete').on('click', function () {
                $('#cart-lateral .footer ul li span.frete .box-2').fadeOut(300, function () {
                    $('#cart-lateral .footer ul li span.frete .box-1').fadeIn(300);
                });
            });
        },

        //CALCULA MANUALMENTE
        calculaFrete: function () {
            //MASK
            $('#search-cep input[type="text"]').mask('00000-000');

            //CLICK
            $('#search-cep input[type=submit]').on('click', function (e) {
                e.preventDefault();
                if ($('#search-cep input[type="text"]').val().length === 9) {
                    functions.calculateShipping();
                    $('#search-cep input[type="text"]').removeClass('active');
                } else {
                    $('#search-cep input[type="text"]').addClass('active');
                }
            });

            //PRESS ENTER
            $('#search-cep input[type=text]').on('keypress', function (event) {
                if (keycode == '13') {
                    if ($('#search-cep input[type="text"]').val().length === 9) {
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        functions.calculateShipping();
                        $('#search-cep input[type="text"]').removeClass('active');
                    } else {
                        $('#search-cep input[type="text"]').addClass('active');
                    }
                }
            });
        },

        //MONTA LISTA DE PRODUTOS
        cartLareal: function () {
            //REMOVE LOADING
            $('#cart-lateral .content').removeClass('loading');

            functions.qtyCart();

            vtexjs.checkout.getOrderForm()
                .done(function (orderForm) {

                    //INFORMACOES DO CARRINHO
                    if (orderForm.value != 0) {
                        total_price = orderForm.value / 100;
                        total_price = total_price.toFixed(2).replace('.', ',').toString();

                        $('#cart-lateral .footer .total-price').text('R$: ' + total_price);
                    } else {
                        $('#cart-lateral .footer .total-price, #cart-lateral .value-frete').text('R$: 0,00');
                    }

                    if (orderForm.totalizers.length != 0) {
                        sub_price = orderForm.totalizers[0].value / 100;
                        sub_price = sub_price.toFixed(2).replace('.', ',').toString();

                        $('#cart-lateral .footer .value-sub-total').text('R$: ' + sub_price);
                    } else {
                        $('#cart-lateral .footer .value-sub-total').text('R$: 0,00');
                    }

                    if (orderForm.items != 0) {
                        //QUANTIDADE DE ITENS NO CARRINHO
                        var total_items = 0;

                        for (var i = orderForm.items.length - 1; i >= 0; i--) {
                            total_items = parseInt(total_items) + parseInt(orderForm.items[i].quantity);
                        }

                        $('#cart-lateral .header .total-items').text(total_items);

                    } else {
                        $('#cart-lateral .header .total-items').text('0');
                    }
                    //FIM - INFORMACOES DO CARRINHO

                    //ITEMS DO CARRINHO
                    $('#cart-lateral .content ul li').remove();
                    for (i = 0; i < orderForm.items.length; i++) {

                        price_item = orderForm.items[i].price / 100;
                        price_item = price_item.toFixed(2).replace('.', ',').toString();

                        var content = '';

                        content += '<li data-index="' + i + '">';
                        content += '<div class="image"><img src="' + orderForm.items[i].imageUrl + '" alt="' + orderForm.items[i].name + '"/></div>';

                        content += '<div class="text">';
                        content += '<p>' + orderForm.items[i].name + '</p>';

                        content += '<ul class="ft">';

                        content += '<li data-index="' + i + '">';
                        content += '<div class="box-count">';
                        content += '<a href="" class="count count-down">-</a>';
                        content += '<input type="number" value="' + orderForm.items[i].quantity + '" />';
                        content += '<a href="" class="count count-up">+</a>';
                        content += '</div>';
                        content += '</li>';

                        content += '<li>';
                        content += '<p class="price">R$: ' + price_item + '</p>';
                        content += '</li>';

                        content += '</ul>';
                        content += '</div>';

                        content += '<span class="removeUni"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" style=" width: 16px; height: 16px; fill: #7d7d7d;"> <g> <g> <path d="m10.5,31.2h4.5l10.5,87.7c0.2,2.1 2,3.6 4.1,3.6h69.8c2.1,0 3.8-1.5 4.1-3.6l10.5-87.7h4.5c2.3,0 4.1-1.8 4.1-4.1s-1.8-4.1-4.1-4.1h-8.2-23.2v-12.5c0-2.3-1.8-4.1-4.1-4.1h-37c-2.3,0-4.1,1.8-4.1,4.1v12.5h-23.2-8.2c-2.3,0-4.1,1.8-4.1,4.1s1.9,4.1 4.1,4.1zm39.6-16.6h28.8v8.4h-28.8v-8.4zm55.6,16.6l-9.9,83.2h-62.6l-9.9-83.2h82.4z"></path> <path d="m50.9,101.9c2.3,0 4.1-1.8 4.1-4.1v-49.1c0-2.3-1.8-4.1-4.1-4.1-2.3,0-4.1,1.8-4.1,4.1v49.1c0,2.3 1.8,4.1 4.1,4.1z"></path> <path d="m78.1,101.9c2.3,0 4.1-1.8 4.1-4.1v-49.1c0-2.3-1.8-4.1-4.1-4.1s-4.1,1.8-4.1,4.1v49.1c0,2.3 1.9,4.1 4.1,4.1z"></path> </g> </g></svg></span>';
                        content += '</li>';

                        $('#cart-lateral .content > ul').prepend(content);
                        functions.barra_frete();
                    }
                    //FIM - ITEMS DO CARRINHO
                });
        },

        //ALTERA QUANTIDADE
        changeQuantity: function () {
            $(document).on('click', '#cart-lateral .count', function (e) {
                e.preventDefault();

                //LOADING
                $('#cart-lateral .content').addClass('loading');

                var qtd = $(this).siblings('input[type="number"]').val();
                if ($(this).hasClass('count-up')) {
                    qtd++
                    $(this).siblings('input[type="number"]').removeClass('active');
                    $(this).siblings('input[type="number"]').val(qtd);
                } else if ($(this).hasClass('count-down')) {
                    if ($(this).siblings('input[type="number"]').val() != 1) {
                        qtd--
                        $(this).siblings('input[type="number"]').val(qtd);
                        //REMOVE ALERTA DE ESTOQUE
                        $('#cart-lateral .no-estoque').removeClass('active');
                    } else {
                        //ALERTA 0 USUARIO QUANTIDADE NEGATIVA
                        $(this).siblings('input[type="number"]').addClass('active');
                    }
                }

                var data_index = $(this).parents('li').data('index');
                data_quantity = $(this).parents('li').find('.box-count input[type="number"]').val();

                vtexjs.checkout.getOrderForm()
                    .then(function (orderForm) {
                        var total_produtos = parseInt(orderForm.items.length);
                        var itemIndex = data_index;
                        item = orderForm.items[itemIndex];

                        vtexjs.catalog.getProductWithVariations(item.productId).done(function (product) {
                            var isnum = parseInt(data_quantity); //QUANTIDADE NO INPUT
                            //QUANTIDADE NO ESTOQUE
                            var sku_atual = product.skus.filter(function (sku) {
                                return sku.skuname === item.name;
                            }); 

                            if (isnum > sku_atual[0].availablequantity) {
                                $(this).parents('li').find('.box-count').addClass('active'); //LOADING
                                $('#cart-lateral .no-estoque').fadeIn(); //ALERTA SOBRE ESTOQUE
                                setTimeout(() => {
                                    $('#cart-lateral .no-estoque').fadeOut();
                                }, 3000); 
                                functions.cartLareal();
                            }

                            vtexjs.checkout.getOrderForm()
                                .then(function (orderForm) {
                                    var itemIndex = data_index;
                                    item = orderForm.items[itemIndex];

                                    var updateItem = {
                                        index: data_index,
                                        quantity: data_quantity
                                    };

                                    return vtexjs.checkout.updateItems([updateItem], null, false);
                                })
                                .done(function (orderForm) {
                                    functions.cartLareal();
                                });
                        });
                    });
            });
        },

        //REMOVE 1 ITEM
        removeItems: function () {
            $(document).on('click', '#cart-lateral .removeUni', function () {
                //LOADING
                $('#cart-lateral .content').addClass('loading');

                var data_index = $(this).parents('li').data('index');
                var data_quantity = $(this).siblings('li').find('.box-count input[type="number"]').val();

                vtexjs.checkout.getOrderForm()
                    .then(function (orderForm) {
                        var itemIndex = data_index;
                        var item = orderForm.items[itemIndex];
                        var itemsToRemove = [{
                            "index": data_index,
                            "quantity": data_quantity,
                        }]
                        return vtexjs.checkout.removeItems(itemsToRemove);
                    })
                    .done(function (orderForm) {
                        functions.cartLareal();
                    });
            });
        },

        //REMOVE ALL
        removeAllItems: function () {
            $('#cart-lateral .clear').on('click', function () {
                //LOADING
                $('#cart-lateral .content').addClass('loading');
                
                vtexjs.checkout.removeAllItems()
                    .done(function (orderForm) {
                        //ATUALIZA O CARRINHO APÓS ESVAZIAR
                        functions.cartLareal();
                    });
            });
        }
    };

    var produto = {
        aviso_cart: function (mensagem) {
            //ABRE CARRINHO FLUTUANTE E EXIBE UMA MENSAGEM
            $('header .ico-cart > a').trigger('click');

            $('#cart-message').append('<li><p>' + mensagem + '</p><span>x</span><a href="/checkout">Abrir carrinho</a></li>');
            $('#cart-message li').fadeIn(300, function () {
                $('#cart-message li span').on('click', function () {
                    $('#cart-message li').fadeOut(300, function () {

                        //REMOVE MENSAGEM E FECHA CARRINHO FLUTUANTE 
                        $('#cart-message li').remove();
                        $('header .ico-cart > a').trigger('click');
                    });
                });
            });

            //SE O USUARIO N FECHAR... SOME
            setTimeout(() => {
                $('#cart-message li').fadeOut(300, function () {
                    $('#cart-message li').remove();
                });
            }, 4000);
        },

        btn_buy: function () {
            $('.buy_in_page').on('click', function (e) {
                e.preventDefault();

                if ($('.sku-selector-container ul.topic.Tamanho li.skuList span label').hasClass('sku-picked')) {
                    //TAMANHO SELECIONADO
                    vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {

                        var number = $('.cta_prod .qty input').val();
                        int = selectedToBuy[0];

                        if (int === undefined) {
                            int = product.skus[0].sku;
                        } else {
                            int = selectedToBuy[0];
                        }

                        //CONSULTA LISTA DE SKUS DO PRODUTO - ESTOQUE
                        var sku_atual = product.skus.filter(function (sku) {
                            return sku.sku == int;
                        });

                        var estoque = sku_atual[0].availablequantity;

                        //ESTOQUE
                        if (number <= estoque) {
                            //REMOVE MENSAGEM SEM ESTOQUE
                            $('.produto .row-1 .no-estoque').removeClass('active');

                            var item = {
                                id: int,
                                quantity: number,
                                seller: '1'
                            };

                            vtexjs.checkout.getOrderForm()
                            vtexjs.checkout.addToCart([item], null, 1)
                                .done(function (orderForm) {
                                    functions.cartLareal();

                                    if ($(document).width() > 768) {
                                        //DEKSTOP: EXIBE UMA  MENSAGEM
                                        produto.aviso_cart('Produto adicionado com sucesso!');
                                    } else {
                                        //MOBILE: SHAKE NO CARRINHO
                                        $('#cart-lateral').addClass('shake');
                                        setTimeout(() => {
                                            $('#cart-lateral').removeClass('shake');
                                        }, 2000);
                                    }
                                });
                        } else {
                            //MENSAGEM SEM ESTOQUE
                            $('.produto .row-1 .no-estoque').fadeIn();

                            setTimeout(() => {
                                $('.produto .row-1 .no-estoque').fadeOut();
                            }, 3000); 
                        }
                    });
                } else {
                    //SELECIONE UM TAMANHO
                    swal("Oops", "Escolha um tamanho!", "error")
                }
            });
        }
    };

    produto.btn_buy();

    functions.barra_frete();
    functions.no_estoque();
    functions.openCart();
    functions.toggleCart();
    functions.automaticCalculateShipping();
    functions.fadeAction();
    functions.calculaFrete();
    functions.cartLareal();
    functions.changeQuantity();
    functions.removeItems();
    functions.removeAllItems();

})();